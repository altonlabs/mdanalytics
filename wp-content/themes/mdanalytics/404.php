<?php get_header(); ?>

<div id="main">
	<div id="twocolumns" class="container">
		<div class="row">
            <div id="content" class="col-sm-8">
                <div <?php post_class(); ?>>
                    <div class="title">
                        <h1><?php _e('Not Found', 'base'); ?></h1>
                    </div>
                    <div class="content">
                        <p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>

			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
