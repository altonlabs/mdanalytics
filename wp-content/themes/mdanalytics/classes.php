<?php
global $drop_class;
//Custom Menu Walker
class Custom_Walker_Nav_Menu extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()) {
		global $drop_class;
		$indent = str_repeat("\t", $depth);
		$output .= "<div class=\"dropdown-menu hidden-sm hidden-xs\"><div class=\"frame\">";
		if($drop_class=='dropdown')  $output .= get_dynamic_sidebar('syndicated-studies-sidebar') ; 
		$output .= "<div class=\"links-area\">";
		$output .= "\n$indent<ul class=\"sub-menu\">\n";
		$drop_class='';
	}

	function end_lvl(&$output, $depth = 0, $args = array()) {
		global $drop_class;
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul></div></div></div>\n";
		$drop_class='';
	}

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		global $drop_class;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$drop_class = $classes[0];

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
		
		if($classes[0]=='ul') $output .= '</ul><ul>';
		
		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		
		$attributes .= ( $classes[0]=='dropdown' )        ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
		

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el(&$output, $object, $depth = 0, $args = array()) {
		$output .= "</li>\n";
	}
}

?>