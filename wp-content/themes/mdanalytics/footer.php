		<footer id="footer">
			<div class="container">
				<?php if(is_active_sidebar('footer-menu-sidebar') || is_active_sidebar('address-sidebar')): ?>
				<div class="row hidden-xs">
					<?php if(is_active_sidebar('footer-menu-sidebar')) dynamic_sidebar('footer-menu-sidebar'); ?>
					<?php if(is_active_sidebar('address-sidebar')) dynamic_sidebar('address-sidebar'); ?>
				</div>
				<?php endif; ?>
				<div class="row info">
					<?php if(is_active_sidebar('footer-sidebar')) dynamic_sidebar('footer-sidebar'); ?>
					<div class="col-sm-4 col-xs-12">
						<strong class="logo"><a href="<?php echo home_url(); ?>">MDAnalytics &reg;</a></strong>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_enqueue_script('jquery'); ?>
		<?php wp_footer(); ?>
		<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.0/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.main.js"></script>
<?php
	if ($_SERVER['SERVER_NAME'] === 'mdanalytics.com' || $_SERVER['SERVER_NAME'] === 'www.mdanalytics.com')
	{
?>
		<!-- Google Analytics -->
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-515970-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>

		<!-- Pardot -->
		<script type="text/javascript">
			piAId = '38252';
			piCId = '1918';

			(function() {
			function async_load(){
			var s = document.createElement('script'); s.type = 'text/javascript';
			s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
			var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
			}
			if(window.attachEvent) { window.attachEvent('onload', async_load); }
			else { window.addEventListener('load', async_load, false); }
			})();
		</script>

<?php
	}
?>
	</body>
</html>

