<?php

//staging restrictions
if (file_exists(sys_get_temp_dir().'/staging-restrictions.php')) {
	define('STAGING_RESTRICTIONS', true);
	require_once sys_get_temp_dir().'/staging-restrictions.php';
}


include( get_template_directory() .'/constants.php' );
include( get_template_directory() .'/classes.php' );
include( get_template_directory() .'/widgets.php' );

add_action('themecheck_checks_loaded', 'theme_disable_cheks');
function theme_disable_cheks() {
	$disabled_checks = array('TagCheck');
	global $themechecks;
	foreach ($themechecks as $key => $check) {
		if (is_object($check) && in_array(get_class($check), $disabled_checks)) {
			unset($themechecks[$key]);
		}
	}
}

add_theme_support( 'automatic-feed-links' );

if ( ! isset( $content_width ) ) $content_width = 900;

//remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');

add_action( 'after_setup_theme', 'theme_localization' );
function theme_localization () {
	load_theme_textdomain( 'base', get_template_directory() . '/languages' );
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'id' => 'default-sidebar',
		'name' => __('Default Sidebar', 'base'),
		'before_widget' => '<section class="archive %2$s" id="%1$s">',
		'after_widget' => '</section>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	register_sidebar(array(
		'id' => 'home-intro-sidebar',
		'name' => __('Home Introduction Sidebar', 'base'),
		'before_widget' => '<div class="col %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'id' => 'mobile-social-sidebar',
		'name' => __('Mobile Social Button Sidebar', 'base'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	register_sidebar(array(
		'id' => 'syndicated-studies-sidebar',
		'name' => __('Syndicated Studies Sidebar', 'base'),
		'before_widget' => '<div class="textbox %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h1>',
		'after_title' => '</h1>'
	));
	register_sidebar(array(
		'id' => 'socialize-us-sidebar',
		'name' => __('Socialize Us Sidebar', 'base'),
		'before_widget' => '<section class="social-area %2$s" id="%1$s"><div class="container">',
		'after_widget' => '</div></section>',
		'before_title' => '<strong class="title">',
		'after_title' => '</strong>'
	));
	register_sidebar(array(
		'id' => 'address-sidebar',
		'name' => __('Address Sidebar', 'base'),
		'before_widget' => '<div class="col-sm-4 address %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'id' => 'newsletter-sidebar',
		'name' => __('Newsletter Sidebar', 'base'),
		'before_widget' => '<section class="newsletter %2$s" id="%1$s"><div class="container">',
		'after_widget' => '</div></section>',
		'before_title' => '<strong class="title">',
		'after_title' => "</strong>\n"
	));
	register_sidebar(array(
		'id' => 'footer-menu-sidebar',
		'name' => __('Footer Menu Sidebar', 'base'),
		'before_widget' => '<nav class="fnav col-sm-4 %2$s" id="%1$s">',
		'after_widget' => '</nav>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'id' => 'footer-sidebar',
		'name' => __('Footer Sidebar', 'base'),
		'before_widget' => '<div class="col-sm-8 col-xs-12 info-area %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'single_post_thumbnail', 1296, 353, true );
}

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'base' ),
) );


//add [email]...[/email] shortcode
function shortcode_email($atts, $content) {
	$result = '';
	for ($i=0; $i<strlen($content); $i++) {
		$result .= '&#'.ord($content{$i}).';';
	}
	return $result;
}
add_shortcode('email', 'shortcode_email');

// register tag [template-url]
function filter_template_url($text) {
	return str_replace('[template-url]',get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// register tag [site-url]
function filter_site_url($text) {
	return str_replace('[site-url]',get_bloginfo('url'), $text);
}
add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');


/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
		$css_classes = str_replace("current-menu-item", "active", $css_classes);
		$css_classes = str_replace("current-menu-parent", "active", $css_classes);
		return $css_classes;
}
add_filter('nav_menu_css_class', 'change_menu_classes');

/* Replace Standart WP Body Classes and Post Classes */
function theme_body_class($classes) {
	if (is_array($classes)) {
		foreach ($classes as $key => $class) {
			$classes[$key] = 'body-class-' . $classes[$key];
		}
	}
	
	return $classes;
}
add_filter('body_class', 'theme_body_class', 9999);

function theme_post_class($classes) {
	if (is_array($classes)) {
		foreach ($classes as $key => $class) {
			$classes[$key] = 'post-class-' . $classes[$key];
		}
	}
	
	return $classes;
}
add_filter('post_class', 'theme_post_class', 9999);

//allow tags in category description
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
	remove_filter($filter, 'wp_filter_kses');
}


//Make WP Admin Menu HTML Valid
function wp_admin_bar_valid_search_menu( $wp_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch"><div>';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" tabindex="10" type="text" value="" maxlength="150" />';
	$form .= '<input type="submit" class="adminbar-button" value="' . __('Search', 'base') . '"/>';
	$form .= '</div></form>';

	$wp_admin_bar->add_menu( array(
		'parent' => 'top-secondary',
		'id'     => 'search',
		'title'  => $form,
		'meta'   => array(
			'class'    => 'admin-bar-search',
			'tabindex' => -1,
		)
	) );
}
function fix_admin_menu_search() {
	remove_action( 'admin_bar_menu', 'wp_admin_bar_search_menu', 4 );
	add_action( 'admin_bar_menu', 'wp_admin_bar_valid_search_menu', 4 );
}
add_action( 'add_admin_bar_menus', 'fix_admin_menu_search' );

/* disable comments on pages by default */
function theme_page_comment_status($post_ID, $post, $update) {
	if (!$update) {
		remove_action('save_post_page', 'theme_page_comment_status', 10);
		wp_update_post(array(
			'ID' => $post->ID,
			'comment_status' => 'closed',
		));
		add_action('save_post_page', 'theme_page_comment_status', 10, 3);
	}
}
add_action('save_post_page', 'theme_page_comment_status', 10, 3);

add_filter('get_avatar','change_avatar_css');

function change_avatar_css($class) {
$class = str_replace("class='avatar", "class='img-rounded", $class) ;
return $class;
}
function get_dynamic_sidebar($index = 1)
{
	$sidebar_contents = "";
	ob_start();
	dynamic_sidebar($index);
	$sidebar_contents = ob_get_clean();
	return $sidebar_contents;
} 


function custom_get_next_post_link( $format = '%link', $link = '%title', $in_same_cat = false, $excluded_terms = '', $taxonomy = 'category' ) {
	return custom_get_adjacent_post_link( $format, $link, $in_same_cat, $excluded_terms, false, $taxonomy );
}

function custom_next_post_link( $format = '%link', $link = '%title', $in_same_cat = true, $excluded_terms = '', $taxonomy = 'category' ) {
	 return custom_get_next_post_link( $format, $link, $in_same_cat, $excluded_terms, $taxonomy );
}

function custom_get_adjacent_post_link( $format, $link, $in_same_cat = false, $excluded_terms = '', $previous = true, $taxonomy = 'category' ) {
	if ( $previous && is_attachment() )
		$post = get_post( get_post()->post_parent );
	else
		$post = get_adjacent_post( $in_same_cat, $excluded_terms, $previous, $taxonomy );
	if ( ! $post ) {
		$output = '';
	} else {
		$title = $post->post_title;
		
		if ( empty( $post->post_title ) )
			$title = $previous ? __( 'Previous Post' ) : __( 'Next Post' );

		/** This filter is documented in wp-includes/post-template.php */
		$title = apply_filters( 'the_title', $title, $post->ID );
		$author_id = $post->post_author; 
		$tags = '';
		$posttags = get_the_tags($post->ID);
		if ($posttags) {
		  $count = 1;
		  foreach($posttags as $tag):
			if($count==1){
				$tags = '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>';
				$count++;
			}else{
				$tags .= ','.'<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>';
			}
		   endforeach;
		}
		
		$output = '<article class="post">
			<header class="header">
				<h3><a href="' . get_permalink( $post ) . '">'.$title.'</a></h3>
				<a href="' . get_permalink( $post ) . '" class="more">More</a>
			</header>
			<footer class="meta">
				<div class="img-area">'. get_avatar( get_the_author_meta( 'ID', $author_id ), 63 ).'</div>
				<div class="textbox">
					<strong class="author"><a href="'. get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) .'">'.get_the_author_meta( 'user_nicename' , $author_id ).'</a></strong>
					<div class="info">
						<time class="date" datetime="'.get_the_time('Y-m-d', $post->ID).'">'.get_the_time('F jS', $post->ID).'</time>
						<div class="tags">
							'.$tags.'
						</div>
					</div>
				</div>
			</footer>
		</article>';

	}
	return $output;
}

function current_url($url)
{
	$url  = isset( $_SERVER['HTTPS'] ) && 'on' === $_SERVER['HTTPS'] ? 'https' : 'http';
	$url .= '://' . $_SERVER['SERVER_NAME'];
	$url .= in_array( $_SERVER['SERVER_PORT'], array('80', '443') ) ? '' : ':' . $_SERVER['SERVER_PORT'];
	$url .= $_SERVER['REQUEST_URI'];

	return $url;
}