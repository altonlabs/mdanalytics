<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta property="og:title" content="<?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?>" />
		<meta property="og:description" content="" />
		<meta property="og:image" content="" />

		<title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
		<link href="<?php echo get_template_directory_uri(); ?>/bootstrap.min.css" rel="stylesheet">
		<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo get_template_directory_uri(); ?>/all.css" rel="stylesheet" media="all">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css"  />

		<script type="text/javascript">
			var pathInfo = {
				base: '<?php echo get_template_directory_uri(); ?>/',
				css: 'css/',
				js: 'js/',
				swf: 'swf/',
			}
		</script>

		<?php if ( is_singular() ) wp_enqueue_script( 'theme-comment-reply', get_template_directory_uri()."/js/comment-reply.js" ); ?>

		<?php wp_head(); ?>

		<!-- TypeKit -->
		<script type="text/javascript" src="//use.typekit.net/kjd2awu.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<!-- Share This -->
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript"> stLight.options({ publisher:&#039;12345&#039; }); </script>

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body <?php if(is_page_template('template-home.php')) echo 'class="home"'; ?>>
		<header id="header">
			<div class="holder container">
				<strong class="logo"><a href="<?php echo home_url(); ?>"><?php echo bloginfo('name'); ?></a></strong>
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-area">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="nav-area">
						<div class="search">
							<a href="#">Search</a>
							<div class="search-content">
								<div class="frame">
									<form role="form">
										<input type="search" placeholder="Type here to Search" class="form-control">
									</form>
								</div>
							</div>
						</div>
						<?php if(has_nav_menu('primary'))
							wp_nav_menu( array('container' => false,
								 'theme_location' => 'primary',
								 'menu_class' => 'nav navbar-nav',
								 'items_wrap' => '<ul class="%2$s">%3$s</ul>',
								 'walker' => new Custom_Walker_Nav_Menu) ); ?>
						<?php  if(is_active_sidebar('mobile-social-sidebar')) dynamic_sidebar('mobile-social-sidebar'); ?>
					</div>
				</nav>
			</div>
		</header>