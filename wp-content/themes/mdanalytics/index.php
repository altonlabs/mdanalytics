<?php get_header(); ?>

<div id="main">
    <div id="twocolumns" class="container">
        <div class="row">
            <div id="content" class="col-sm-8">
                <?php if (have_posts()) : ?>
            
                <?php while (have_posts()) : the_post(); ?>
                <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                    <div class="title">
                        <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                        <p class="info"><strong class="date"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_time('F jS, Y') ?></a></strong> <?php _e('by', 'base'); ?> <?php the_author(); ?></p>
                    </div>
                    <div class="content">
                        <?php the_excerpt(); ?>
                    </div>
                    <div class="meta">
                        <ul>
                            <li><?php _e('Posted in', 'base'); ?> <?php the_category(', ') ?></li>
                            <li><?php comments_popup_link(__('No Comments', 'base'), __('1 Comment', 'base'), __('% Comments', 'base')); ?></li>
                            <?php the_tags(__('<li>Tags: ', 'base'), ', ', '</li>'); ?>
                            <?php edit_post_link( __( 'Edit', 'base' ), '<li>', '</li>' ); ?>
                        </ul>
                    </div>
                </div>
                <?php endwhile; ?>
                
                <div class="navigation">
                    <div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
                    <div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
                </div>
                
                <?php else : ?>
                <div class="post">
                    <div class="head">
                        <h1><?php _e('Not Found', 'base'); ?></h1>
                    </div>
                    <div class="content">
                        <p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
