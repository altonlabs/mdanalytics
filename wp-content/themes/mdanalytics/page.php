<?php get_header(); ?>

<div id="main">
	<div id="twocolumns" class="container">
		<div class="row">
            <div id="content" class="col-sm-8">
            
                <?php if (have_posts()) : ?>
                
                <?php while (have_posts()) : the_post(); ?>
                <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                    <div class="title">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="content">
                        <?php the_content(); ?>
                        <?php wp_link_pages(); ?>
                        <?php edit_post_link( __( 'Edit', 'base' ), '<div class="edit-link">', '</div>' ); ?>
                    </div>
                </div>
                <?php endwhile; ?>
                
                <?php comments_template(); ?>
                
                <?php else : ?>
                <div class="post">
                    <div class="title">
                        <h1><?php _e('Not Found', 'base'); ?></h1>
                    </div>
                    <div class="content">
                        <p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
                
            </div>

			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
