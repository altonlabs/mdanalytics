<?php get_header(); ?>

<div id="main">
	<div id="twocolumns" class="container">
		<div class="row">
            <div id="content" class="col-sm-8">
                <?php if (have_posts()) : ?>
            
                <div <?php post_class(); ?>>
                    <div class="title">
                        <h1><?php printf( __( 'Search Results for: %s', 'base' ), '<span>' . get_search_query() . '</span>'); ?></h1>
                    </div>
                </div>
            
                <?php while (have_posts()) : the_post(); ?>
                <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                    <div class="title">
                        <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    </div>
                    <div class="content">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
                <?php endwhile; ?>
                
                <div class="navigation">
                    <div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
                    <div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
                </div>
                
                <?php else : ?>
                <div class="post">
                    <div class="title">
                        <h2><?php _e('No posts found.', 'base'); ?></h2>
                    </div>
                    <div class="content">
                        <p><?php _e('Try a different search?', 'base'); ?></p>
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <?php endif; ?>
                
            </div>

			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
