<aside id="sidebar" class="col-md-3 col-md-push-1 col-sm-12">
    <div class="sidebar-holder">
        <?php dynamic_sidebar('default-sidebar'); ?>
        <?php $post_id = get_queried_object_id(); ?>
		<?php $cat_id = wp_get_post_categories( $post_id ); ?>
        <?php if($cat_id): ?>
        <?php query_posts( array( 'category__and' => $cat_id, 'posts_per_page' => 4 ,'post__not_in' => array($post_id)) ); ?>
        <?php if(have_posts()): ?> 
        <section class="post-list">
            <ul>	
                <?php while(have_posts()): the_post(); ?>
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
            </ul>
        </section>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        <?php endif; ?>
    </div>
</aside>

