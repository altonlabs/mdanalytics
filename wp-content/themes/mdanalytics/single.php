<?php get_header(); ?>

<div id="main">
	<?php if(have_posts()): ?>
	<?php while(have_posts()): the_post(); ?>
	<?php $p_id = get_the_ID(); ?>
	<?php $cat_ids = wp_get_post_categories( $p_id ); ?>
	<section class="post-section add">
		<div class="img-holder"><?php if(has_post_thumbnail()) the_post_thumbnail('single_post_thumbnail'); ?></div>
		<div class="holder">
			<div class="container">
				<div class="row">
					<article class="post col-md-8 col-sm-12">
						<div class="post-holder">
							<header class="header">
								<strong class="title">
									<?php $count = 1; $category='';?>
									<?php foreach($cat_ids as $cat_id):?>
									<?php if($count==1){ ?>
									<?php $category = get_cat_name($cat_id); ?>
									<?php }else{?>
									<?php $category .= ', '.get_cat_name($cat_id); ?>
									<?php } ?>
									<?php $count++; endforeach; ?>
									<?php echo $category; ?>
								</strong>
							</header>
							<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						</div>
						<footer class="meta">
							<div class="img-area"><?php echo get_avatar(get_the_author_meta('ID'),60); ?></div>
							<div class="textbox">
								<strong class="author"><a href=" <?php echo get_author_posts_url( get_the_author_meta('ID') ); ?> "><?php the_author(); ?></a></strong>
								<div class="info">
									<time class="date" datetime="<?php the_time('Y-m-d') ?>"><?php the_time('F jS') ?></time>
									<?php the_tags(__('<div class="tags">', 'base'), ', ', '</div>'); ?>
								</div>
							</div>
						</footer>
					</article>
				</div>
			</div>
		</div>
	</section>
	<div id="twocolumns" class="container">
		<div class="row">
			<div id="content" class="col-md-8 col-sm-12">
				<article class="article">
					<?php the_content(); ?>
				</article>
			</div>
			<div class="social-holder add col-sm-12 visible-tablets">
				<div class="holder">
					<strong class="title">Share this post:</strong>
					<ul class="social-networks-list">
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<?php endwhile; ?>
	<div class="container">
		<?php comments_template(); ?>
	</div>
	<div class="social-holder visible-desktops">
		<div class="container">
			<strong class="title">Share this post:</strong>
			<ul class="social-networks-list">
				<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
			</ul>
		</div>
	</div>
	<?php else: ?>
	<div id="twocolumns" class="container">
		<div class="row">
			<div id="content" class="col-sm-8">
				<h1><?php _e('Not Found', 'base'); ?></h1>
				<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<?php endif; ?>
	<?php if(custom_next_post_link()): ?>
	<section class="post-section">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<div class="post-holder">
						<strong class="title">Read Next</strong>
						<?php echo custom_next_post_link(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<?php if(is_active_sidebar('newsletter-sidebar')) dynamic_sidebar('newsletter-sidebar'); ?>
</div>
	
<?php get_footer(); ?>
