<?php
/*
Template Name: Home Template
*/
get_header(); ?>

<?php if(is_active_sidebar('home-intro-sidebar')): ?>
<section class="intro container">
	<?php dynamic_sidebar('home-intro-sidebar'); ?>
</section>
<?php endif; ?>
<div id="main">
	<section class="features container">
		<?php if(have_posts()): ?>
		<?php while(have_posts()): the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
		<?php else: ?>
		<p><?php _e("Sorry, but you are looking for something that isn't here.", 'base'); ?></p>
		<?php endif; ?>
	</section>
	<?php query_posts('cat='.syndicatedstudiesCategoryID.'&showposts=-1');?>
	<?php if(have_posts()): ?>
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<?php $class = 'class="active"'; $count=0; ?>
			<?php while(have_posts()): the_post(); ?>
			<li data-target="#carousel-example-generic" data-slide-to="<?php echo $count; ?>" <?php echo $class; $class='' ?>></li>
			<?php $count++; endwhile; ?>
		</ol>
		<div class="carousel-inner">
			<?php $class = 'active'; ?>
			<?php while(have_posts()): the_post(); ?>
			<div class="item <?php echo $class; $class=''; ?>">
				<?php if(has_post_thumbnail()):?>
				<?php $thumb_id = get_post_thumbnail_id(); ?>
				<?php $attach = get_post($thumb_id); ?>
				<img src="<?php echo wp_get_attachment_url($thumb_id); ?>"  alt="<?php echo $attach->post_title; ?>" class="">
				<?php endif; ?>
				<div class="carousel-caption">
					<div class="caption-holder">
						<header class="header">
							<strong class="subhead"><?php echo get_cat_name(syndicatedstudiesCategoryID); ?></strong>
							<time class="date" datetime="<?php the_time('Y-m-d') ?>"><?php the_time('F jS') ?></time>
							<h1><?php the_title(); ?></h1>
						</header>
						<?php the_content(); ?>
						<a href="<?php the_permalink(); ?>" class="btn btn-primary">READ STUDY</a>
						<span class="note">or visit the  <a href="<?php echo get_category_link( syndicatedstudiesCategoryID ); ?>"><?php echo get_cat_name(syndicatedstudiesCategoryID); ?> section</a></span>
					</div>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
		<a class="left carousel-control hidden-xs" href="#carousel-example-generic" data-slide="prev">Previous</a>
		<a class="right carousel-control hidden-xs" href="#carousel-example-generic" data-slide="next">Next</a>
	</div>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
	<?php query_posts('cat='.featurednewsCategoryID.'&showposts=2');?>
	<?php if(have_posts()): ?>
	<section class="news">
		<div class="container">
			<h3><?php echo get_cat_name(featurednewsCategoryID); ?></h3>
			<div class="row holder">
				<?php $counter = 1; ?>
				<?php while(have_posts()): the_post(); ?>
				<article class="post col-md-5 col-sm-6 col-xs-12 <?php if($counter>1) echo 'col-md-push-2 hidden-xs'; ?>">
					<header class="header">
						<time class="date" datetime="<?php the_time('Y-m-d') ?>"><?php the_time('F jS') ?></time>
						<h3><a class="blog-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</header>
					<?php the_excerpt(); ?>
					<a href="<?php the_permalink(); ?>" class="btn btn-default">Learn More</a>
				</article>
				<?php $counter++; endwhile; ?>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
	<?php if(is_active_sidebar('socialize-us-sidebar')) dynamic_sidebar('socialize-us-sidebar'); ?>
</div>

<?php get_footer(); ?>
